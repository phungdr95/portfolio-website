import React from "react";
import "./portfolio.css";
import IMG1 from "../../assets/the-band.jpg";
import IMG2 from "../../assets/tiki.png";
import IMG3 from "../../assets/f8_icon.png";
import IMG4 from "../../assets/music-phayer.png";

const data = [
  {
    id: 1,
    image: IMG1,
    title: "The Band",
    gitlab: "https://gitlab.com/phungdr95/study",
    demo: "https://the-band-ten.vercel.app/",
  },
  {
    id: 2,
    image: IMG2,
    title: "Shop Online",
    gitlab: "https://gitlab.com/phungdr95/shop-online",
    demo: "https://shop-online-orcin.vercel.app/",
  },
  {
    id: 3,
    image: IMG3,
    title: "F8 Website",
    gitlab: "https://gitlab.com/phungdr95/f8-fullstack",
    demo: "https://f8-fullstack.vercel.app/",
  },
  {
    id: 4,
    image: IMG4,
    title: "Music Player",
    gitlab: "https://gitlab.com/phungdr95/music-player",
    demo: "https://music-player-delta-five.vercel.app/",
  }
]

const Portfolio = () => {
  return (
    <section id="portfolio">
      <h5>My Recent Work</h5>
      <h2>Projects</h2>
      <div className="container portfolio__container">
        {data.map(({ id, image, title, gitlab, demo }) => {
          return (
            <article key={id} className="portfolio__item">
              <div className="portfolio__item-image">
                <img src={image} alt={title} />
              </div>
              <h3>{title}</h3>
              <div className="portfolio__item-cta">
                <a href={gitlab} className="btn" target="_blank">Gitlab</a>
                <a href={demo} className="btn btn-primary" target="_blank">Live Demo</a>
              </div>
            </article>
          );
        })}
      </div>
    </section>
  );
};

export default Portfolio;
