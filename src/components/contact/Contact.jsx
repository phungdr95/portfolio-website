import React from 'react';
import { MdOutlineEmail } from 'react-icons/md';
import { RiMessengerLine } from 'react-icons/ri';
import { SiZalo } from 'react-icons/si';
import { useRef } from 'react';
import emailjs from 'emailjs-com';

import './contact.css';

const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm(
      process.env.REACT_APP_MAIL_SERVICE_ID,
      process.env.REACT_APP_MAIL_TEMPLATE_ID,
      form.current,
      process.env.REACT_APP_MAIL_USER_ID
    );

    e.target.reset();
  };

  return (
    <section id="contact">
      <h5>Get In Touch</h5>
      <h2>Contact Me</h2>

      <div className='container contact__container'>
        <div className='contact__options'>
          <article className='contact__option'>
            <MdOutlineEmail className='contact__option-icon' />
            <h4>Email</h4>
            <h5>phungdr95@gmail.com</h5>
            <a href='mailto:phungdr95@gmail.com' target='_blank' rel="noreferrer">
              Send a message
            </a>
          </article>
          <article className='contact__option'>
            <RiMessengerLine className='contact__option-icon' />
            <h4>Messenger</h4>
            <h5>Phụng Huỳnh</h5>
            <a href='https://m.me/phung.dante' target='_blank' rel="noreferrer">
              Send a message
            </a>
          </article>
          <article className='contact__option'>
            <SiZalo />
            <h4>Zalo</h4>
            <h5>+84362999495</h5>
            {/* <a href="" target="_blank">Send a message</a> */}
          </article>
        </div>
        {/*  END CONTACT OPTIONS */}
        <form ref={form} onSubmit={sendEmail}>
          <input
            type='text'
            name='name'
            placeholder='Your full Name'
            required
          />
          <input type='email' name='email' placeholder='Your Email' required />
          <textarea
            name='message'
            rows='7'
            placeholder='Your Message'
            required
          ></textarea>
          <button type='submit' className='btn btn-primary'>
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
