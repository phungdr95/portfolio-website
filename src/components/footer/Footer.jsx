import React from "react";
import "./footer.css";
import { FaFacebookF } from "react-icons/fa";
import { FiInstagram } from "react-icons/fi";
import { SiZalo } from "react-icons/si";
import { SiVercel } from "react-icons/si";
import { FiGitlab } from "react-icons/fi";

const Footer = () => {
  return (
    <footer>
      <ul className="permalinks">
        <li>
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#about">About</a>
        </li>
        <li>
          <a href="#experience">Experience</a>
        </li>
        <li>
          <a href="#services">Services</a>
        </li>
        <li>
          <a href="#portfolio">Projects</a>
        </li>
        {/* <li><a href='#testimonials'>Testimonials</a></li> */}
        <li>
          <a href="#contact">Contact</a>
        </li>
      </ul>

      <div className="footer__socials">
        <a href="https://facebook.com" target="_blank" rel="noreferrer">
          <FaFacebookF />
        </a>
        <a href="https://instagram.com" target="_blank" rel="noreferrer">
          <FiInstagram />
        </a>
        <a href="https://id.zalo.me/" target="_blank" rel="noreferrer">
          <SiZalo />
        </a>
        <a href="https://id.zalo.me/" target="_blank" rel="noreferrer">
          <SiVercel />
        </a>
        <a href="https://id.zalo.me/" target="_blank" rel="noreferrer">
          <FiGitlab />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
