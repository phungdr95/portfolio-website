import React from 'react';

import {
  Header,
  Nav,
  About,
  Experience,
  // Services,
  Portfolio,
  Contact,
  Footer,
  // Testimonials,
} from './components';

const App = () => {
  return (
    <>
      <Header />
      <Nav />
      <About />
      <Experience />
      {/* <Services /> */}
      <Portfolio />
      {/* <Testimonials /> */}
      <Contact />
      <Footer />
    </>
  );
};

export default App;
